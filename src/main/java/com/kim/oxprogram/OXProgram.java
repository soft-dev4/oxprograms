/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.kim.oxprogram;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class OXProgram {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }
    }

    private static void showTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.println(table[r][c]);
            }
            System.out.println("");
        }
    }

    private static void showWelcome() {
        System.out.println("Welcom to OX Game");
    }

    private static void showTurn() {
        System.out.println("Turn" + " " + currentPlayer);
    }

    private static void inputRowCol() {
        System.out.println("Please input row,col:");
        row = kb.nextInt();
        col = kb.nextInt();
    }

    private static void process() {
        if (setTable()) {
            if (checkWin()) {
                finish = true;
                ShowWin();
                return;
            }
            count++;
            if (checkDraw()) {
                finish = true;
                showDraw();
                return;
            }
            swicthPlayer();
        }
    }

    private static void ShowWin() {
        showTable();
        System.out.println(">>>" + currentPlayer + " " + "Win<<<");
    }

    public static void swicthPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }

    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;

    }

    private static boolean checkWin() {
        if (checkVertical()) {
            return true;
        } else if (checkHorizon()) {
            return true;
        } else if (checkX()) {
            return true;
        }
        return false;
    }

    private static boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkHorizon() {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;

    }

    private static boolean checkX1() {// 11 ,22, 33
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() { // 13, 22,31 => 02 11,20
        for (int i = 0; i < table.length; i++) {
            if (table[i][2-i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDraw() {
        if (count == 9) {
            return true;
        }
        return false;
    }

    private static void showDraw() {
        showTable();
        System.out.println(">>> DRAW <<<");
    }
}
